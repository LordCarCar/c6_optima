#pragma once

#include "main.h"

#define SCR_DECODE 		0x09
#define SCR_INTENSITY 	0x0A
#define SCR_SCAN 		0x0B
#define SCR_SHUTDOWN 	0x0C
#define SCR_TEST 		0x0F

#define LED_ON			0x01
#define LED_OFF			0x00

#define SVC_CMD			0x00
#define SVC_CHAR		0x01
#define SVC_CH_ATTR		0x02
#define SVC_LED			0x03
#define SVC_LD_ATTR		0x04

u32 getSysTick(void);

void scr_init(void);
void scr_cmd(u8 cmd, u8 arg);
void scr_char(int idx, u8 data);
void scr_ch_attr(int idx, u8 attr);
void scr_led(int idx, u8 state);
void scr_ld_attr(int idx, u8 attr);
