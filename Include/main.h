#pragma once

#include "stm32f10x_conf.h"
#include "stdbool.h"

signed int printf(const char *pFormat, ...);
