#include "scr_drv.h"

void scr_init(void) {
	NVIC_SetPriority(SVCall_IRQn, 12);
	NVIC_SetPriority(PendSV_IRQn, 12);

	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA | RCC_APB2Periph_GPIOB | RCC_APB2Periph_SPI1, ENABLE);

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN_FLOATING;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_12 | GPIO_Pin_13;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3 | GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_Out_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_10MHz;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_2;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_7;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	SPI_InitTypeDef SPI_InitStruct;
	SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master;
	SPI_InitStruct.SPI_DataSize = SPI_DataSize_16b;
	SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStruct.SPI_NSS = SPI_NSS_Hard;
	SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_16;
	SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStruct.SPI_CRCPolynomial = 7;
	SPI_Init(SPI1, &SPI_InitStruct);
	SPI_SSOutputCmd(SPI1, ENABLE);
	SPI_Cmd(SPI1, ENABLE);

	scr_cmd(SCR_TEST, 0x00);
	scr_cmd(SCR_DECODE, 0x3F);
	scr_cmd(SCR_INTENSITY, 0x7);
	scr_cmd(SCR_SCAN, 0x07);
	scr_cmd(SCR_SHUTDOWN, 0x01);

	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_TimeBaseInitStruct.TIM_Period = 3125 - 1;
	TIM_TimeBaseInitStruct.TIM_Prescaler = 5 - 1;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStruct.TIM_RepetitionCounter = 0x0000;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStruct);
	TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);

	NVIC_SetPriority(TIM3_IRQn, 11);
	NVIC_EnableIRQ(TIM3_IRQn);

	TIM_Cmd(TIM3, ENABLE);
}

static u8 scr_buffer[8] = { 0 };

static u8 scr_tick = 0;
static u8 scr_data[8] = { 0 };
static u8 scr_attr[22] = { 0 };

void scr_proc(int idx) {
	u8 data;
	if (idx < 6) {
		data = (scr_tick & scr_attr[idx]) ? 0x0F : scr_data[idx];
	} else {
		u8 *attr = scr_attr + 6;
		if (idx > 6)
			attr += 8;
		u8 mask = 0;
		for (int i = 0; i < 8; i++) {
			mask >>= 1;
			if (!(scr_tick & attr[i]))
				mask |= 0x80;
		}
		data = scr_data[idx] & mask;
	}
	if (data != scr_buffer[idx]) {
		scr_buffer[idx] = data;
		while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)) {
		}
		SPI_I2S_SendData(SPI1, ((idx + 1) << 8) | data);
	}
}

bool pendsv_scr_flag = 0;

bool pendsv_btn_flag = 0;
u16 tim3_btn_state = 0;
u32 tim3_btn_time;

void PendSV_Handler(void) {
	if (pendsv_scr_flag) {
		pendsv_scr_flag = 0;
		for (int i = 0; i < 8; i++)
			scr_proc(i);
	}
	if (pendsv_btn_flag) {
		pendsv_btn_flag = 0;
		printf("btn: %04X, time: %6d\r\n", tim3_btn_state, tim3_btn_time);
	}
}

static u32 systick;
void TIM3_IRQHandler(void) {
	systick++;
	if ((systick & 0x3F) == 0) {
		scr_tick++;
		pendsv_scr_flag = 1;
		SCB->ICSR |= SCB_ICSR_PENDSVSET;
	}
	u16 btn = ((~GPIOA->IDR >> 5) & 0x0180) | (~GPIOB->IDR & 0x7F);
	if (btn != tim3_btn_state) {
		tim3_btn_state = btn;
		tim3_btn_time = systick;
		pendsv_btn_flag = 1;
		SCB->ICSR |= SCB_ICSR_PENDSVSET;
	}
	TIM_ClearITPendingBit(TIM3, TIM_IT_Update);
}

u32 getSysTick(void) {
	return systick;
}

static int svc_cmd;
static int svc_idx;
static u8 svc_data;

void SVC_Handler(void) {
	int idx = svc_idx;
	switch (svc_cmd) {
	case SVC_CMD:
		while (SPI_I2S_GetFlagStatus(SPI1, SPI_I2S_FLAG_BSY)) {
		}
		SPI_I2S_SendData(SPI1, (svc_idx << 8) | svc_data);
		return;
	case SVC_CHAR:
		scr_data[idx] = svc_data;
		break;
	case SVC_CH_ATTR:
		scr_attr[idx] = svc_data;
		break;
	case SVC_LED:
		idx = 6 + idx / 8;
		if (svc_data) {
			scr_data[idx] |= (1 << (svc_idx & 0x07));
		} else {
			scr_data[idx] &= ~(1 << (svc_idx & 0x07));
		}
		break;
	case SVC_LD_ATTR:
		scr_attr[idx + 6] = svc_data;
		idx = 6 + idx / 8;
		break;
	}
	scr_proc(idx);
}

void scr_cmd(u8 cmd, u8 arg) {
	if (cmd > 8 && cmd < 16) {
		svc_cmd = SVC_CMD;
		svc_idx = cmd;
		svc_data = arg;
		__asm("svc 0");
	}
}

void scr_char(int idx, u8 data) {
	if (idx < 8) {
		svc_cmd = SVC_CHAR;
		svc_idx = idx;
		svc_data = data;
		__asm("svc 0");
	}
}

void scr_ch_attr(int idx, u8 attr) {
	if (idx < 6) {
		svc_cmd = SVC_CH_ATTR;
		svc_idx = idx;
		svc_data = attr;
		__asm("svc 0");
	}
}

void scr_led(int idx, u8 state) {
	if (idx < 16) {
		svc_cmd = SVC_LED;
		svc_idx = idx;
		svc_data = state;
		__asm("svc 0");
	}
}

void scr_ld_attr(int idx, u8 attr) {
	if (idx < 16) {
		svc_cmd = SVC_LD_ATTR;
		svc_idx = idx;
		svc_data = attr;
		__asm("svc 0");
	}
}
